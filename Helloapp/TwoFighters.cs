﻿using System;

public class TwoFighters
{
    public class Fighter
    {
        public string Name;
        public int Health, DamagePerAttack;
        public Fighter(string name, int health, int damagePerAttack)
        {
            this.Name = name;
            this.Health = health;
            this.DamagePerAttack = damagePerAttack;
        }
    }

    public static Fighter fighter1 = new Fighter("Harry", 10, 1);
    public static Fighter fighter2 = new Fighter("Herald", 10, 1);

    public static string DeclareWinner(Fighter fighter1, Fighter fighter2, string firstAttacker)
    {
        string winner = null;
        int j = fighter1.Health;

        if (firstAttacker == fighter2.Name)
        {
            for (int i = fighter2.Health; ; i -= fighter1.DamagePerAttack)
            {


                if (i <= 0)
                {
                    winner = fighter1.Name;
                    break;
                }

                else if (fighter1.Health > 0)
                {
                    j -= fighter2.DamagePerAttack;

                    if (j <= 0)
                    {
                        winner = fighter2.Name;
                        break;
                    }
                }
            }
        }

        else if (firstAttacker == fighter1.Name)
        {
            for (int i = fighter2.Health - fighter1.DamagePerAttack; ; i -= fighter1.DamagePerAttack)
            {


                if (i <= 0)
                {
                    winner = fighter1.Name;
                    break;
                }

                else if (fighter1.Health > 0)
                {
                    j -= fighter2.DamagePerAttack;

                    if (j <= 0)
                    {
                        winner = fighter2.Name;
                        break;
                    }
                }
            }
        }

        else
        {
            Console.WriteLine("Choose first fighter");
        }

            return winner;
    }
}
