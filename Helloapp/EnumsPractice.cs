﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Helloapp
{
    class EnumsPractice
    {
        enum Days
        {
            Monday = 1,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
            public string GetWeekDayName(int weekDay) 
            {      
                return Enum.GetName(typeof(Days), weekDay);
            }        
    }
}
