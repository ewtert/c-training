﻿using System;

public class Arrays_Practice
{
    public static int sumTwoSmallestNumbers(int[] numbers)
    {
        int firstNumber = numbers[0];
        int secondNumber = numbers[1];
        for (int i = 2; i < numbers.Length; i++)
        {
            if (numbers[i] < firstNumber || numbers[i] < secondNumber)
            {
                if (firstNumber > secondNumber)
                {
                    firstNumber = numbers[i];
                }
                else
                {
                    secondNumber = numbers[i];
                }
            }
        }

        return firstNumber + secondNumber;
    }


    public int CountNotEvenElements()
    {
        int[] numbers = { 1, 2, 3, 4, 5 };
        int result = 0;

        for (int i = 0; i < numbers.Length; i++)
        {
            if (i % 2 != 0)
            {
                result += numbers[i];
            }
            else { }
        }

        return result;
    }


    public int GetIndexMaxElementInArray()
    {
        int[] numbers = { 1, 2, 3, 4, 5 };

        return numbers.Length - 1;
    }


    public int GetMinElementInArray()
    {
        int[] numbers = { 1, 2, 3, 4, 5 };
        int maxNumber = 0;

        foreach (int i in numbers)
        {
            if(i > maxNumber)
            {
                maxNumber = i;
            }
        }

        return maxNumber;
    }

    
    public void GetArrayElementsForeach()
	{
        int[,,] numbers = { { { 1, 2 },{ 3, 4 } },
                { { 4, 5 }, { 6, 7 } },
                { { 7, 8 }, { 9, 10 } },
                { { 10, 11 }, { 12, 13 } }
              };
        foreach (int i in numbers)
        {
            Console.Write($"{i} ");
        }
    }
}
