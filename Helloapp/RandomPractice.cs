﻿using System;
using MyLib;

namespace Helloapp
{
	public class RandomPractice
	{
		public static string NoSpace(string input)
		{
			return input.Replace(" ", string.Empty);
		}

		public static string BreakCamelCase(string str)
		{
			string result = null;
			for (int i = 0; i < str.Length; i++)
			{
				if (char.IsUpper(str[i]))
				{
					result += " ";
				}
				result += str[i];
			}

			return result;
		}
	}
}