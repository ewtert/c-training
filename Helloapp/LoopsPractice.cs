﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Helloapp
{
class LoopsPractice
    {
        public static long[] SumDigPow(long a, long b)
        {
            //Task 17. 

            long indexConvertedNumber;
            List<long> eureka = new List<long>();

            for (long j = a; j <= b; j++) //Check in future realization of range based on IEnumerable 
            {
                string convertedNumber = Convert.ToString(j);
                long result = 0;

                for (int i = 1; i <= convertedNumber.Length; i++) // check how can we user here foreach
                {
                    indexConvertedNumber = long.Parse(convertedNumber[i-1].ToString());
                    result += (long)Math.Pow(indexConvertedNumber, i);
                }

            if (result == j)
                {
                    eureka.Add(result);
                }
            }

            return eureka.ToArray();
        }


        public static string RepeatStr(int n, string s)
        {
            // Task 16. Write a function called repeatString which repeats the given String src exactly count times.

            string result = "";
            for (int i = 0; i < n; i++)
            {
                result += s;
            }

            return result;
        }


        public void GetMirroredNumber(string n)
        {
            // Task 15. This app will show a mirror of entered number

            for (int i = n.Length - 1; i >= 0; i--)
            {
                Console.Write(n[i]);
            }
        }


        public int GetSumOfDigits(string n)
        {
            // Task 14. This app will count a sum of digits for entered number

            int sum = 0;
            for (int i = 0; i < n.Length; i++)
            {
                sum += int.Parse(n[i].ToString()); //need to understand what this record n[i].ToString() means - used hint from website. Do n.Length starts from 0? Do Parse n[i] starts from 1?
            }

            return sum;
        }


        public void GetAvarageOfNotPrimeNumbers()
        {
            // Task 13. This app will show avarage of all not prime numbers till number you entered
            int counter = 0;
            int divider = 0;
            decimal result;
            int limitNumber = 0;
            GetValidatedNumber("a number to define all not prime numbers till this number", ref limitNumber);            

            for (int i = 0; i <= limitNumber; i++)
            {
                if (!GetPrime(i))
                {
                    divider++;
                    counter += i;
                }
            }
            result = counter / divider;
            Console.WriteLine($"Average number for all not prime numbers till {limitNumber} is {result}");
        }


        public int GetFactorial(int number)
        {
            // Task 12. This app will count a factorial of the number 

            int i = 1;
            int n = 1;
            if (number == 0)
            {
                n *= i;
            }

            while (i <= number)
            {
                n *= i;
                i++;
            }

            return n;
        }


        public void GetPythagoreanTheorem()
        {
            // Task 11. This app will count all right triangles with maximum size of hypotenuse 1000

            int firstCathet;
            int secondCathet;
            int hypotenuse = 1;
            const int HypotenuseLimit = 1000;

            while (hypotenuse <= HypotenuseLimit)
            {
                for (firstCathet = 1; firstCathet <= HypotenuseLimit; firstCathet++)
                {
                    for (secondCathet = 1; secondCathet <= HypotenuseLimit; secondCathet++)
                    {
                        if ((firstCathet*firstCathet)+(secondCathet*secondCathet) == hypotenuse * hypotenuse)
                        {
                            Console.WriteLine($"{firstCathet}^2 * {secondCathet}^2 = {hypotenuse}^2");                            
                        }
                    }
                }
                hypotenuse++;
            }
        }


        public void GetSqRootLoop()
        {
            // Task 10. This app will find a root square of the number with help of loop up to 100

            int number = 0;
            GetValidatedNumber("a number to define its square root", ref number);

            while (true)
            {
                int option = 0;
                GetValidatedNumber("1 for sequential search option OR enter 2 for binary search option", ref option);

                if (option == 1)
                {
                    int i = 1;
                    while (true)
                    {
                        if (i * i == number)
                        {
                            Console.Write($"Square root of {number} is {i}");
                            break;
                        }

                        i++;
                    }
                }

                if (option == 2)
                {
                    int min = 1;
                    const int Max = 100;
                    int mid = Max / 2;                                       
                    int midSq = mid * mid;
                    int minSq = min * min;
                    
                    while (true)
                    {
                        if (minSq == number)
                        {
                            Console.Write($"Square root of {number} is {min}");
                        }

                        else if (midSq == number)
                        {
                            Console.Write($"Square root of {number} is {mid}");
                        }

                        else if (midSq > number)
                        {
                            if (minSq < number)
                            {
                                min++;
                            }
                            else
                            {
                                Console.WriteLine($"There is no Square root for this number. Error in if (midSq > number) for min: {min} min: {mid}");
                            }
                        }

                        else if (midSq < number)
                        {
                            if (midSq < number)
                            {
                                mid++;
                            }
                            else
                            {
                                Console.WriteLine($"There is no Square root for this number. Error in else if (midSq < number) for min: {min} min: {mid}");
                            }
                        }

                        else
                        {
                            Console.WriteLine($"There is no square root for this number. Error in While (true) for min: {min} min: {mid}");
                        }
                    }
                }
            }
        }


        public void GetDividers()
        {
            // Task 9. This app will show all dividers for an entered number

            int number = 0;
            GetValidatedNumber("a number to define its deviders", ref number);

            for (int i = 1; i <= number; i++)
            {
                if (number % i == 0)
                {
                    Console.Write($"{i} ");
                }
            }
        }


            public void GetMoreLessSquareNumbers()
        {
            // Task 8. This app will show numbers, if square of them more and less than entered number

            int number = 0;
            GetValidatedNumber("a number, and the system will show numbers, if square of them more and less than entered number (not more than 100)", ref number);            

            for(int i = 0; i < 100; i++)
            {
                if (i * i < number)
                {
                    Console.WriteLine($"Square of {i} is less than {number}");
                }
                else if (i * i > number)
                {
                    Console.WriteLine($"Square of {i} is more than {number}");
                }
            }
        }

        public void GetListOfPrimes()
        {
            // Task 7. This app will show a list of prime numbers

            int number = 0;
            GetValidatedNumber("a number, and the system will show a list of prime numbers till this number", ref number);
              
            for (int i = 0; i <= number; i++)
            {
                if (GetPrime(i))
                {
                    Console.Write($"{i} ");
                }
            }                      
        }


        public void GetPalindrome()
        {
            // Task 6. This app will define whether the number is a palindrome or not

            GetValue("a palindrome number");
            string number = Console.ReadLine();

            int i = 0, j = number.Length - 1;
            while (i < j && number[i] == number[j]) // number [i] how does it work?
                {
                    i++; j--; 
                }

            if (number[i] == number[j])
            {
                Console.WriteLine($"{number} - this is a palindrome");
            }

            else
            {
                Console.WriteLine($"{number} - this is NOT a palindrome");
            }
        }


        public void GetChessBoard()
        {
            // Task 5. This app will draw a chess board

            int columns = 6;
            int rows = 4;
            for (int i = 0; i < rows; i++)
            {
                {
                    for (int j = 0; j <= columns; j++)
                    {
                        if (i % 2 == 0)
                        {
                            Console.Write("*  ");
                            if (j == columns)
                            {
                                Console.Write("\n");
                            }
                        }
                        else
                        {
                            Console.Write("  *");
                            if (j == columns)
                            {
                                Console.Write("\n");
                            }                            
                        }
                    }
                }               
            }
        }


        public void GetStarsSquare()
        {
            // Task 4. This app will draw a stars square

            int waymark = 5;
            for (int i = 0; i <= waymark; i++)
            {
                if (i == 0 || i == 5)
                {
                    Console.WriteLine("******");
                }
                else
                {
                    Console.WriteLine("*    *");
                }
            }
        }

        public bool GetPrime(int x)
        {
            // Task 3. This function will define whether the number is prime or not

            if (x == 2)
            {
                return true;
            }
            if (x < 2 || x % 2 == 0)
            {
                return false;
            }
            for (int i = 3; i <= Math.Sqrt(x); i += 2)
            {
                if (x % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public void GetSumAndCountOfEvenLoop()
        {
            // Task 2. This app will calculate sum of even numbers and count amount of even numbers

            int evenSum = 0;
            int evenCount = 0;

            int i = 0;
            while (i <= 99)
            {
                if (i%2 == 0)
                {
                    evenSum += i;
                    evenCount++;
                }
                i++;
            }
            Console.WriteLine($"Sum of even digits is {evenSum} \nCount of even digits is {evenCount}");
        }


        public void GetDigitsMultiplication()
        {
            // Task 1. This app will multiply two numbers

            int multiplicand;
            int factor;
            int result;
            while (true)
            {
                GetValue("a multiplicand in diapason 0 - 10: ");
                string inputMultiplicand = Console.ReadLine();
                bool checkMultiplicand = int.TryParse(inputMultiplicand, out multiplicand);
                if (checkMultiplicand == false || multiplicand < 0 || multiplicand > 10)
                    {
                        Console.WriteLine("You entered a wrong number");
                        continue;
                    }

                Console.WriteLine("Enter a factor in diapason 0 - 10: ");
                string inputFactor = Console.ReadLine();
                bool checkFactor = int.TryParse(inputFactor, out factor);
                if (checkFactor == false || factor < 0 || factor > 10)
                    {
                        Console.WriteLine("You entered a wrong number");
                        continue;
                    }

                result = multiplicand * factor;
                Console.WriteLine($"The multiplication result is {result}");
                break;
            }
        }


        public void GetMultiplicationTable()
        {
            // Task 0. This app will show total amount of money including percentage

            int multiplicand = 1;
            int factor = 1;
            while (multiplicand <= 10)
            {
                int result = multiplicand * factor;
                Console.WriteLine($"Result of multiplication {multiplicand} and {factor} is {result}");
                if (factor == 10)
                {
                    factor = factor - 10;
                    multiplicand++;
                }
                factor++;
            }
        }

        
        public void GetDepositFinalAmountWhile()
        {
            // Task -1.

            Console.WriteLine("This app will total amount of money including percentage");

            GetValue("amount of money: ");
            decimal money = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Enter duration: ");
            int duration = Convert.ToInt32(Console.ReadLine());
            const decimal Percentage = 0.07m;
            int i = duration;

            while (i > 0)
            {
                money += money * Percentage;
                i--;
            }
            Console.WriteLine($"After {duration} month amount of money become {money}");
            Console.ReadKey();
        }


        public void GetDepositFinalAmount()
        {
            //Task -2. This app will show total amount of money including percentage

            GetValue("amount of money: ");
            decimal money = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Enter duration: ");
            int duration = Convert.ToInt32(Console.ReadLine());
            const decimal Percentage = 0.07m;

            for (int i = 1; i <= duration; i++)
            {
                money += money * Percentage;
            }
            Console.WriteLine($"After {duration} month amount of money become {money}");
            Console.ReadKey();
        }


        public void ShowArray(long[] a)
        {
            foreach (int i in a)
            {
                Console.WriteLine(i);
            }
        }


        static void GetValue(string freeText) => Console.WriteLine($"Please enter {freeText}");

        public int GetValidatedNumber(string freeText, ref int number)
        {
            while (true)
            {
                
                GetValue(freeText);
                string inputNumber = Console.ReadLine();

                bool checkNumber = int.TryParse(inputNumber, out number);

                if (checkNumber == false)
                {
                    Console.WriteLine("You entered a wrong number");
                    continue;
                }
                else
                {
                    return number;
                }
            }
        }
    }        
}