﻿using System;

namespace Helloapp
{
    class Program
    {
        static void Main(string[] args)
        {
            var enumsPractice = new EnumsPractice();
            var randomPractice = new RandomPractice();
            var twoFighters = new TwoFighters();
            var Arrays_Practice = new Arrays_Practice();

            /*Create a function that will put a space in case if the letter is Uppercase
            Console.Write(RandomPractice.BreakCamelCase("testTextText"));*/

            /*Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers.
             No floats or non-positive integers will be passed.
            int[] numbersArray = { 5, 8, 12, 19, 22 };
            Console.WriteLine(Arrays_Practice.sumTwoSmallestNumbers(numbersArray));*/

            /*Create a function that returns the name of the winner in a fight between two fighters.
            Console.WriteLine(TwoFighters.DeclareWinner(TwoFighters.fighter1, TwoFighters.fighter2, "Herald"));*/

            //3) Посчитать сумму элементов массива с нечетными индексами
            //Console.WriteLine(Arrays_Practice.CountNotEvenElements());

            //2) Найти индекс максимального элемента массива
            //Console.WriteLine(Arrays_Practice.GetIndexMaxElementInArray());

            //1) Найти минимальный элемент массива
            //Console.WriteLine(Arrays_Practice.GetMinElementInArray());
            //Loops topic practice
            //var LoopsPractice = new LoopsPractice();

            /*17) The number 89 is the first integer with more than one digit that fulfills the property partially introduced in the title of this kata.
            What's the use of saying "Eureka"? Because this sum gives the same number.
            In effect: 89 = 8 ^ 1 + 9 ^ 2
            The next number in having this property is 135.
            See this property again: 135 = 1 ^ 1 + 3 ^ 2 + 5 ^ 3
            We need a function to collect these numbers, that may receive two integers a, b that defines the range[a, b](inclusive) and outputs a list
            of the sorted numbers in the range that fulfills the property described above.*/
            //LoopsPractice.ShowArray(LoopsPractice.SumDigPow(1, 100));

            /*16) Write a function called repeatString which repeats the given String src exactly count times.
            Console.Write(LoopsPractice.RepeatStr(5, "*"));*/

            //15) Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например,
            //задано число 123, вывести 321.
            //LoopsPractice.GetMirroredNumber("54321");

            //14) Посчитать сумму цифр заданного числа
            //Console.WriteLine(LoopsPractice.GetSumOfDigits("112"));

            //13) Вычислить среднее значение чисел, не являющихся простыми, в диапазоне от 1 до n.
            //LoopsPractice.GetAvarageOfNotPrimeNumbers();

            //12) Вычислить факториал числа n. n! = 1 * 2 *…*n - 1 * n;
            //Console.WriteLine(LoopsPractice.GetFactorial(5));

            /*11) Теорема Пифагора: вывести все прямоугольные треугольники, стороны которых – натуральные числа меньше 1000, посчитать их количество
(по теореме Пифагора у прямоугольного треугольника сумма квадратов катетов равна квадрату гипотенузы).Подсказка: использовать полный перебор
с трижды вложенными циклами.*/
            //LoopsPractice.GetPythagoreanTheorem();

            //10) Найти корень натурального числа с точностью до целого (рассмотреть переборный вариант, и метод бинарного поиска)
            //LoopsPractice.GetSqRootLoop();

            //9) Вывести все делители числа
            //LoopsPractice.GetDividers();

            //8) Вывести все натуральные числа,  квадрат которых больше/меньше заданного числа n
            //LoopsPractice.GetMoreLessSquareNumbers();

            //7) Вывести все простые числа в диапазоне от 2 до n
            //LoopsPractice.GetListOfPrimes();

            /*6) Напишите программу, которая определяет, является ли введенное число палиндромом ? 
                (Палиндром – число или текст, которые одинаково читаются слева направо и справа налево)*/
            //LoopsPractice.GetPalindrome();

            /*5) Вывести шахматную доску с заданными размерами, по принципу:
                        *  *  *  *  *  *
                          *  *  *  *  *  *
                        *  *  *  *  *  *
                          *  *  *  *  *  *          */
            //LoopsPractice.GetChessBoard();

            /*4) Вывести на экран квадрат, со стороной а
                 *****
                 *   *
                 *   *
                 *   *
                 *****  */
            //LoopsPractice.GetStarsSquare();

            //3) Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)
            //LoopsPractice.GetPrime();

            //2) Найти сумму четных чисел и их количество в диапазоне от 1 до 99
            //LoopsPractice.GetSumAndCountOfEvenLoop();

            /*1) Напишите программу, в которую пользователь вводит два числа и выводит результат их умножения.
 При этом программа должны запрашивать у пользователя ввод чисел, пока оба вводимых числа не окажутся в диапазоне от 0 до 10.
 Если введенные числа окажутся больше 10 или меньше 0, то программа должна вывести пользователю о том, что введенные числа
 недопустимы, и повторно запросить у пользователя ввод двух чисел. Если введенные числа принадлежат диапазону от 0 до 10,
 то программа выводит результат умножения. Для организации ввода чисел используйте бесконечный цикл while и оператор break.*/
            // LoopsPractice.GetDigitsMultiplication();   

            /*0) Напишите программу, которая выводит на консоль таблицу умножения */
            //LoopsPractice.GetMultiplicationTable();

            /*-1. Перепишите предыдущую программу, только вместо цикла for используйте цикл while.*/
            //LoopsPractice.GetDepositFinalAmountWhile();


            /*-2. За каждый месяц банк начисляет к сумме вклада 7% от суммы. Напишите консольную программу, в которую пользователь вводит сумму
 вклада и количество месяцев. А банк вычисляет конечную сумму вклада с учетом начисления процентов за каждый месяц.
Для вычисления суммы с учетом процентов используйте цикл for. Для ввода суммы вклада используйте выражение
Convert.ToDecimal(Console.ReadLine()) (сумма вклада будет представлять тип decimal). */
            //LoopsPractice.GetDepositFinalAmount();

            /*Simple, remove the spaces from the string, then return the resultant string.
            Console.Write(RandomPractice.NoSpace("8 j 8   mBliB8g  imjB8B8  jl  B"));*/
            /* Чем ошибочна следующая запись?
            while (i > 0) sum += i;
             a) {} is abcent
             b) there is not increment here, therefore no loop*/

            /*Show all elements in array with foreach
            Arrays_Practice.GetArrayElementsForeach();*/

            /*Написать функцию, которая возвращает строковое название дня недели по номеру дня.
            EnumsPractice.GetWeekDayName(3);*/
        }
    }
}
